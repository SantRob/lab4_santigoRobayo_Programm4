package jala.university;

import java.util.Comparator;

/**
 * Student.
 */
public class Student implements Sortable{
    private String name;
    private int grade;

    public Student(String name, int grade) {
        this.name = name;
        this.grade = grade;
    }


    public static Comparator<Student> byGrade() {
            return byGrade();
    }

    @Override
    public String toString() {
        return "{" + name +"," + grade +'}';
    }

    @Override
    public void sort() {
        
    }

    @Override
    public void sortBy(Comparator comparator) {

    }
}