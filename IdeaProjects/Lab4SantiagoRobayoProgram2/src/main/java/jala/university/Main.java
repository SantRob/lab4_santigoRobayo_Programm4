package jala.university;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> list = new UniArrayList<>();
        var size = 1_000_000;

        for (int i = 0; i < size; i++) {
            list.add(Integer.toString(i));
            System.out.println(i);
        }

    }
}
