package jala.university;

import java.util.Iterator;
import java.util.List;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Collection;
import java.util.ListIterator;
import java.util.Comparator;

public class UniArrayList <T> implements List <T>, Sortable, Unique {

    private Object[] array;
    private int size;


    public UniArrayList() {
        this.array = new Object[0];
        this.size = 0;
    }

    public UniArrayList(T[] elements) {
        this.array = new Object[elements.length];
        for (int i = 0; i < elements.length; i++) {
            this.array[i] = elements[i];
        }
        this.size = elements.length;
    }

    public Object[] getArray() {
        return array;
    }

    public void setArray(Object[] array) {
        this.array = array;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return Arrays.toString(array);
    }

    private void additionalCapacity(int minCapacity) {
        if (minCapacity > array.length && minCapacity < 100) {
            Object[] newArray = new Object[minCapacity];
            for (int i = 0; i < array.length; i++) {
                newArray[i] = array[i];
            }
            array = newArray;
        } else if (minCapacity == 100) {
            Object[] newArray = new Object[1_000_000];
            for (int i = 0; i < array.length; i++) {
                newArray[i] = array[i];
            }
            array = newArray;
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            private int currentIndex = 0;

            @Override
            public boolean hasNext() {
                return currentIndex < size;
            }

            @Override
            public T next() {
                if (hasNext()) {
                    return (T) array[currentIndex++];
                } else {
                    throw new NoSuchElementException();
                }
            }
        };
    }


    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean add(T t) {
        additionalCapacity(size+1);
        array[size] = t;
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        Object[] newArray = new Object[size-1];
        int index = 0;
        for (int i = 0; i < array.length; i++) {
            if (!(o.equals(array[i]))){
                newArray[index] = array[i];
                index++;
            }
        }
        size--;
        array = newArray;
        return true;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public T get(int index) {
        return (T) array[index];
    }

    @Override
    public T set(int index, T element) {
        return null;
    }

    @Override
    public void add(int index, T element) {
        additionalCapacity(size + 1);
        for (int i = size; i > index; i--) {
            array[i] = array[i - 1];
        }
        array[index] = element;
        size++;
    }


    @Override
    public T remove(int index) {
        Object[] newArray = new Object[size-1];
        T element = (T) array[index];
        int position = 0;
        for (int i = 0; i < array.length; i++) {
            if (!(element.equals(array[i]))){
                newArray[position] = array[i];
                position++;
            }
        }
        size--;
        array = newArray;
        return element;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public void sort() {

    }

    @Override
    public void sortBy(Comparator comparator) {

    }

    @Override
    public void unique() {

    }
}