package jala.university;

import java.util.Comparator;

/**
 * Student.
 */
public class Student{
    private String name;
    private int grade;

    public Student(String name, int grade) {
        this.name = name;
        this.grade = grade;
    }

    public static Comparator<Student> byGrade() {
        return Comparator.comparingInt(Student::getGrade);
    }

    @Override
    public String toString() {
        return "{" + name +"," + grade +'}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }
}