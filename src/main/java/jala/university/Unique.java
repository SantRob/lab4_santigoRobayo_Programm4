package jala.university;

/**
 * Unique.
 */
public interface Unique<T> {
    void unique();
}